import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AlbumListComponent from '../components/AlbumListComponent';
import AlbumPhotosComponent from '../components/AlbumPhotosComponent';
import PhotoDetails from '../components/PhotoDetails';

const StackNavigator = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName="AlbumList"
      screenOptions={{
        headerShown: true,
        headerStyle: {
          backgroundColor: '#728c69',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <Stack.Screen
        name="AlbumList"
        component={AlbumListComponent}
        options={{
          title: 'Album List',
        }}
      />
      <Stack.Screen
        name="AlbumPhotos"
        component={AlbumPhotosComponent}
        options={{title: 'Photos'}}
      />
      {
        <Stack.Screen
          name="PhotoDetails"
          component={PhotoDetails}
          options={{title: 'Photo Details'}}
        />
      }
    </Stack.Navigator>
  );
};

export default StackNavigator;
