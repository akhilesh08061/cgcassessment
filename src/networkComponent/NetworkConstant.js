// Contants for network calls and api addresses.

// API ADDRESS.
export const API_URL = 'https://jsonplaceholder.typicode.com';

//GET ALBUM LIST
export const GET_ALBUM = '/albums';

//GET PHOTOS LIST FROM SELECT ALBUM
export const GET_PHOTOS = 'photos';
