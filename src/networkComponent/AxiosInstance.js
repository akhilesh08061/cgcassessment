import Axios from 'axios';
import {API_URL} from './NetworkConstant';

export const AxiosInstance = Axios.create({
  baseURL: API_URL,
  headers: {
    'Content-type': 'Application/json',
  },
});
