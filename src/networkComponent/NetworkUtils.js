import * as NetworkConstant from './NetworkConstant';
import {AxiosInstance} from './AxiosInstance';

export const getAlbumList = () => {
  return AxiosInstance.get(NetworkConstant.GET_ALBUM)
    .then(response => {
      return response;
    })
    .catch(error => {
      return error.response;
    });
};

export const getPhotos = albumId => {
  console.log(
    'item id issue',
    `/albums/${albumId}/${NetworkConstant.GET_PHOTOS}`,
  );
  return AxiosInstance.get(`/albums/${albumId}/${NetworkConstant.GET_PHOTOS}`)
    .then(response => {
      console.log('response', response);
      return response;
    })
    .catch(error => {
      console.log('error value', error);
      return error.response;
    });
};
