import {StyleSheet} from 'react-native';
const AlbumPhotoComponentStyle = StyleSheet.create({
  AlbumItemView: {
    flex: 1,
    flexDirection: 'column',
    margin: 2,
  },

  ListStyle: {
    margin: 3,
  },

  AlbumListItemStyle: {
    height: 100,
  },
});

export default AlbumPhotoComponentStyle;
