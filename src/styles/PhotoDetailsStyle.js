import {StyleSheet} from 'react-native';
const PhotoDetailComponentStyle = StyleSheet.create({
  editTitleButton: {
    height: 50,
    width: 200,
    borderRadius: 10,
    backgroundColor: '#78D0E7',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },

  editTitleText: {
    fontSize: 20,
    color: '#000000',
  },

  titleAndOtherDetailHolderView: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 10,
  },

  titleText: {
    fontSize: 28,
    fontWeight: 'bold',
    color: '#000000',
  },

  titleValue: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
  },
});

export default PhotoDetailComponentStyle;
