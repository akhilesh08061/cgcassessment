import {StyleSheet} from 'react-native';
const AlbumComponentStyle = StyleSheet.create({
  AlbumItemView: {
    flex: 1,
    flexDirection: 'column',
    margin: 2,
    backgroundColor: '#a0a299',
  },

  ListStyle: {
    margin: 3,
  },

  AlbumListItemStyle: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default AlbumComponentStyle;
