//import liraries
import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import Svg, {Path} from 'react-native-svg';

// Loader component.
const Loader = props => {
  return (
    <View style={LoaderStyle.container}>
      <View style={LoaderStyle.loaderView}>
        <ActivityIndicator
          animating={props.shouldLoad}
          color="#ed6663"
          size={47}
          style={LoaderStyle.activityIndicatorStyle}
        />
      </View>
    </View>
  );
};

// define your styles
const LoaderStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  loaderView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

//make this component available to the app
export default Loader;
