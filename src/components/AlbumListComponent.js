import {
  SafeAreaView,
  View,
  FlatList,
  TouchableOpacity,
  Text,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import CommonStyle from '../styles/CommonStyles';
import AlbumListStyle from '../styles/AlbumListStyle';
import {getAlbumList} from '../networkComponent/NetworkUtils';
import Loader from './LoaderComponent';

const AlbumList = ({navigation}) => {
  const [albumList, setAlbumList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getAlbumList().then(response => {
      if (response != null) {
        setIsLoading(false);
        setAlbumList(response.data);
      }
    });
  }, []);

  _onPress = item => {
    navigation.navigate('AlbumPhotos', {
      itemId: item.id,
    });
  };

  renderItemList = item => {
    return (
      <TouchableOpacity
        style={AlbumListStyle.AlbumItemView}
        onPress={() => _onPress(item)}>
        <View style={AlbumListStyle.AlbumListItemStyle}>
          <Text style={CommonStyle.textStyle}>{item.id}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  if (isLoading) {
    return <Loader shouldLoad={isLoading} />;
  } else {
    return (
      <SafeAreaView style={CommonStyle.container}>
        <FlatList
          data={albumList}
          renderItem={({item}) => renderItemList(item)}
          //Setting the number of column
          numColumns={3}
          style={AlbumListStyle.ListStyle}
          keyExtractor={(item, index) => index}
        />
      </SafeAreaView>
    );
  }
};

export default AlbumList;
