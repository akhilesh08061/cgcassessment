import React, {useState} from 'react';
import {Image, Text, View, TouchableOpacity} from 'react-native';
import Dialog from 'react-native-dialog';
import PhotoDetailComponentStyle from '../styles/PhotoDetailsStyle';

const PhotoDetailComponent = ({route}) => {
  const {imageUrl} = route.params;
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState(route.params.title);

  showEditDialog = () => {
    setVisible(true);
  };

  const handleOkay = () => {
    setVisible(false);
  };

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1, margin: 10}}>
        <Image style={{flex: 1}} source={{uri: imageUrl}} />
      </View>

      <View style={PhotoDetailComponentStyle.titleAndOtherDetailHolderView}>
        <Text style={PhotoDetailComponentStyle.titleText}>Title </Text>
        <Text style={PhotoDetailComponentStyle.titleValue}>{title}</Text>

        <View style={{flex: 1}}>
          <TouchableOpacity
            style={PhotoDetailComponentStyle.editTitleButton}
            onPress={() => showEditDialog()}>
            <Text style={PhotoDetailComponentStyle.editTitleText}>
              Edit title
            </Text>
          </TouchableOpacity>
        </View>

        {/* show dialog     */}
        <View>
          <Dialog.Container visible={visible}>
            <Dialog.Title>Edit Title</Dialog.Title>
            <Dialog.Input value={title} onChangeText={text => setTitle(text)} />
            <Dialog.Button label="Okay" onPress={handleOkay} />
          </Dialog.Container>
        </View>
      </View>
    </View>
  );
};

export default PhotoDetailComponent;
