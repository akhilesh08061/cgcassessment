import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  TouchableOpacity,
  View,
} from 'react-native';
import {getPhotos} from '../networkComponent/NetworkUtils';
import AlbumPhotoComponentStyle from '../styles/AlbumPhotosStyle';
import CommonStyle from '../styles/CommonStyles';
import Loader from './LoaderComponent';

const AlbumPhotos = ({route, navigation}) => {
  const [albumPhotos, setAlbumPhotoList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const {itemId} = route.params;

  useEffect(() => {
    getPhotos(itemId).then(response => {
      if (response != null) {
        setIsLoading(false);
        setAlbumPhotoList(response.data);
      }
    });
  }, []);

  _onPressPhotoDetails = item => {
    navigation.navigate('PhotoDetails', {
      title: item.title,
      imageUrl: item.url,
    });
  };

  renderItemList = item => {
    return (
      <TouchableOpacity
        style={AlbumPhotoComponentStyle.AlbumItemView}
        onPress={() => _onPressPhotoDetails(item)}>
        <View>
          <Image
            style={AlbumPhotoComponentStyle.AlbumListItemStyle}
            source={{uri: item.thumbnailUrl}}
          />
        </View>
      </TouchableOpacity>
    );
  };

  if (isLoading) {
    return <Loader shouldLoad={isLoading} />;
  } else {
    return (
      <SafeAreaView style={CommonStyle.container}>
        <FlatList
          data={albumPhotos}
          renderItem={({item}) => renderItemList(item)}
          //Setting the number of column
          numColumns={3}
          style={AlbumPhotoComponentStyle.ListStyle}
          keyExtractor={(item, index) => index}
        />
      </SafeAreaView>
    );
  }
};

export default AlbumPhotos;
